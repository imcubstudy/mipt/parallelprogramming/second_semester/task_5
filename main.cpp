// Task 5:
//     - Write a multi-threaded program that will calculate a sum of the series
//     - number of terms is CLI argument
//     - bonus:
//         - choose a series more complex than exp
// Comment: bonus task is done! I've chosen pi
// Author: Gazizov Yakov

#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <omp.h>

// Leibniz formula for pi
double getSeriesTermN(std::uint64_t const N) noexcept
{
    return 2.0 / ((4.0 * N + 1.0) * (4.0 * N + 3.0));
}

int main(int const argc, char const *const argv[])
{
    // get the N as argument from CLI
    std::uint64_t N = 0;
    if (argc != 2) {
        std::printf("Argument is not supplied! Or maybe just too mush of them...\n");
        std::exit(EXIT_FAILURE);
    }
    if (auto ret = std::sscanf(argv[1], "%llu", &N); !(ret != EOF && ret == 1) || N < 1) {
        std::printf("Invalid argument was supplied!\n");
        std::exit(EXIT_FAILURE);
    }

    // set dynamic to false just in case if implementation will allocate different number of threads
    // on every other run
    omp_set_dynamic(static_cast<int>(false));

    // output maximum available number of threads
    auto const maxThreads = omp_get_max_threads();
    std::printf("Max threads %d\n", maxThreads);

    // declare sum
    double pi_4 = 0.0;
    // calculate series
    #pragma omp parallel for default(none) schedule(dynamic, 42) reduction(+: pi_4) firstprivate(N)
    for (std::uint64_t i = 0; i < N; ++i) {
        pi_4 += getSeriesTermN(i);
    }

    // output result
    std::printf("Calculated pi value is %.10lf\n", 4.0 * pi_4);

    return 0;
}
