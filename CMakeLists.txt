cmake_minimum_required(VERSION 3.17)
project(task_5 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(OpenMP REQUIRED)

add_executable(task_5
    main.cpp
)
target_link_libraries(task_5 PRIVATE OpenMP::OpenMP_CXX)
